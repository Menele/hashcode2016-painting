#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <set>
#include <list>
#include <string>
using namespace std;

class Command
{
public:
   Command() {};
   virtual string toString() = 0;
   virtual void apply(vector<string> &wall) = 0;
};

class CommandSquare : public Command
{
public:
   int col;
   int row;
   int size;

   CommandSquare(int c, int r, int s) : col(c), row(r), size(s) {}

   virtual string toString()
   {
      stringstream ss;
      ss << "PAINT_SQUARE " << row << " " << col << " " << size;
      return ss.str();
   }

   virtual void apply(vector<string> &wall)
   {
      // paint
      for (int x = col - size; x <= col + size; ++x)
      {
         for (int y = row - size; y <= row + size; ++y)
         {
            wall[y][x] = '#';
         }
      }
   }
};

class CommandLine : public Command
{
public:
   int c1;
   int r1;
   int c2;
   int r2;

   CommandLine(int cc1, int rr1, int cc2, int rr2) : c1(cc1), r1(rr1), c2(cc2), r2(rr2) {}

   virtual string toString()
   {
      stringstream ss;
      ss << "PAINT_LINE " << r1 << " " << c1 << " " << r2 << " " << c2;
      return ss.str();
   }

   virtual void apply(vector<string> &wall)
   {
      for (int r = r1; r <= r2; ++r)
      {
         for (int c = c1; c <= c2; ++c)
            wall[r][c] = '#';
      }
   }

};

class CommandErase : public Command
{
public:
   int col;
   int row;

   CommandErase(int c, int r) : col(c), row(r){}

   virtual string toString()
   {
      stringstream ss;
      ss << "ERASE_CELL " << row << " " << col;
      return ss.str();
   }

   virtual void apply(vector<string> &wall)
   {
      wall[row][col] = '.';
   }
};

class Surface
{
public:
   int rows, cols;
   vector< vector<bool> > model;
   vector< vector<bool> > painted;
   vector<string> modstr;
   int paintedCells;
   // list of erase to perform 
   set<pair<int, int> > eraseToPerform;

   list<Command*> commands;

   Surface(int r, int c) : rows(r), cols(c)
   {
      paintedCells = 0;
      model.resize(c, vector<bool>(r, false));
      painted = model;
   }
   void readInput(ifstream &in)
   {
      int nLine = 0;
      string line;

      while (nLine < rows)
      {
         in >> line;
         modstr.push_back(line);
         int col = 0;
         for (char c : line)
         {
            if (c == '#')
            {
               ++paintedCells;
               model[col][nLine] = true;
            }
            ++col;
         }
         ++nLine;
      }
   }

   // perform the erase at the end
   list<Command*> addErase()
   {
      list<Command*> er;
      for (auto cell : eraseToPerform)
      {
         Command *c = new CommandErase(cell.first, cell.second);
         er.push_back(c); // cell(column, row)
      }
      return er;
   }

   // check if it's useful to paint a square with top left corner in row,col
   Command* checkSquare(int row, int col)
   {
      return nullptr;

      // assume row,col is painted
      int size = 1;
      int emptyCells = 0;

      int keptSize = 1;
      bool tryBigger = true;
      while (tryBigger)
      {
         size += 2;
         // check size limits
         if (row + size >= rows || col + size >= cols){
            tryBigger = false;
            break;
         }

         // check new cells
         int clearCell = 0;
         // horizontal
         for (int r = row + size - 2; r <= row + size - 1; ++r)
         {
            for (int c = col; c <= col + size - 2; ++c)
            {
               if (!model[c][r])
                  ++clearCell;
            }
         }
         // vertical
         for (int c = col + size - 2; c <= col + size - 1; ++c)
         {
            for (int r = row; r < row + size; ++r)
            {
               if (!model[c][r])
                  ++clearCell;
            }
         }
         emptyCells += clearCell;

         // FOR THE MOMENT DO NOT USE DELETE
         if (emptyCells > 0/*size*/)
         {
            break;
         }
         else
         {
            keptSize = size;
         }
      }

      if (keptSize > 1)
      {
         int s = (keptSize - 1) / 2;
         // mark painted area as already painted
         // TODO use proper method, do not update the model itself !! 
         for (int c = col; c < col + 2 * s + 1; ++c)
         {
            for (int r = row; r < row + 2 * s + 1; ++r)
               model[c][r] = false;
         }
         // TODO
         return new CommandSquare(col + s, row + s, s);
      }

      return nullptr;
   }

   // analyse model to get what to do
   // first : only build horizontal 'lines'
   list<Command*> buildLines()
   {
      list<Command*> commands;
      int l = 0;
      while (l < rows)
      {
         bool painted = false;
         int c = 0;
         int startPaint = 0;
         while (c < cols)
         {
            if (model[c][l])
            {
               // try to paint a sqare ?
               Command *sq = checkSquare(l, c);
               if (sq != nullptr)
               {
                  commands.push_back(sq);
                  CommandSquare *sqc = dynamic_cast<CommandSquare*> (sq);
                  int s = sqc->size * 2 + 1;
                  // paint
                  /* for (int x = c; x < c + s; ++x)
                  {
                  for (int y = l; y < l + s; ++y)
                  {
                  model[x][y] = false;
                  }
                  }*/
                  continue; // do not increment c
               }

               if (!painted) {
                  startPaint = c;
                  painted = true;
               }
            }
            else if (painted)
            {
               // check if better to paint vertical
               int sizePainted = c - 1 - startPaint;
               int ll = l;
               while (ll < rows && model[startPaint][ll])
               {
                  ++ll;
               }
               // TODO find a better way to check if it's interesting or not to use a vertical line
               if (ll - 1 - l > sizePainted + 50)
               {
                  // paint vertical
                  commands.push_back(new CommandLine(startPaint, l, startPaint, ll - 1));
                  int lc = l;
                  while (lc < ll)
                  {
                     // mark as already painted
                     // TODO DO IT PROPERLY !!!
                     // TODO use proper method, do not update the model itself !! 
                     model[startPaint][lc] = false;
                     ++lc;
                  }
                  c = startPaint;
               }
               else
               {
                  commands.push_back(new CommandLine(startPaint, l, c - 1, l));
               }
               painted = false;
            }
            ++c;
         }
         if (painted)
         {
            // check if better to paint vertical
            int sizePainted = c - 1 - startPaint;
            int ll = l;
            while (ll < rows && model[startPaint][ll])
            {
               ++ll;
            }
            // TODO find a better way to check if it's interesting or not to use a vertical line
            if (ll - 1 - l > sizePainted + 50)
            {
               // paint vertical
               commands.push_back(new CommandLine(startPaint, l, startPaint, ll - 1));
               int lc = l;
               while (lc < ll)
               {
                  // mark as already painted
                  // TODO DO IT PROPERLY !!!
                  // TODO use proper method, do not update the model itself !! 
                  model[startPaint][lc] = false;
                  ++lc;
               }
            }
            else
            {
               commands.push_back(new CommandLine(startPaint, l, c - 1, l));
            }

         }

         ++l;
      }
      return commands;
   }


};


class Validator
{
public:
   int rows, cols;
   vector<string> wall;

   Validator(int r, int c) : rows(r), cols(c)
   {
      wall.resize(r, string(c, '.'));
   }

   void applyCommands(list<Command*> commands)
   {
      for (auto com : commands)
      {
         com->apply(wall);
      }
   }

   void dumpWall()
   {
      std::string outputFile("C:\\Users\\dfleury\\Downloads\\dumpcheck.out");
      ofstream out;
      out.open(outputFile.c_str());
      for (string s : wall)
      {
         out << s << endl;
      }
   }
};

int main(int argc, char** argv)
{
   cout << "test" << endl;
   std::string inputFile("C:\\Users\\dfleury\\Downloads\\logo.in");
   std::string outputFile("C:\\Users\\dfleury\\Downloads\\logo.out");

   ofstream out;
   ifstream in;

   in.open(inputFile.c_str());
   out.open(outputFile.c_str());

   int rows, cols;

   in >> rows;
   in >> cols;

   Surface s(rows, cols);
   s.readInput(in);
   list<Command*> c = s.buildLines();
   out << c.size() << endl;
   for (auto comm : c)
   {
      out << comm->toString() << endl;
   }

   // generate the output of the commands to check if it matches the input
   Validator val(rows, cols);
   val.applyCommands(c);
   val.dumpWall();

   if (val.wall != s.modstr)
   {
      cerr << "Error validating output" << endl;
   }

}